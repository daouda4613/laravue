<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('tasksliste/{q?}', 'TaskController@index');
Route::post('addnewtask', 'TaskController@store');
Route::get('task/edit/{id}', 'TaskController@edit');
Route::patch('task/edit/{id}', 'TaskController@update');
Route::delete('task/delete/{id}', 'TaskController@destroy');